image: php:7.2-cli
services:
    - mysql:latest test
#comment for test jenkins    
cache:
    paths:
    - vendor/

artifacts:
    expire_in: 10 day
    paths:
      - vendor/
      - app/config/parameters.yml
      - var/bootstrap.php.cache
  cache:
    paths:
      - /cache/composer
      - ./vendor
before_script:
  - curl -sS https://getcomposer.org/installer | php
  - php composer.phar install
  - php app/console doctrine:database:create --env=test --if-not-exists
  - php app/console doctrine:schema:create --env=test

 script:
    - echo 'memory_limit = 2048M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini
    - composer install --optimize-autoloader
    - php bin/console doctrine:schema:drop --force
    - php bin/console doctrine:schema:create
    - php bin/console doctrine:fixtures:load --no-interaction
    
test:app:
  script:
  - phpunit -c app --debug