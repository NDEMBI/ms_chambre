<?php
//test CRUD API
namespace App\Tests\Controller;

use App\Entity\Chambre;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
Use App\DataFixtures\AppFixtures;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

//test
class ChambreControllerTest extends KernelTestCase

{ 
    /**
     * @var EntityManager
     */
    private $manager;    

      public  function setUp():void
    {    
        $kernel = self::bootKernel(); 
        $this->manager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        
        $loader = new Loader();
        $loader->addFixture(new AppFixtures);
        //On vide les fixtures de la table
        $purger = new ORMPurger($this->manager);
        $executor = new ORMExecutor($this->manager, $purger);
        $executor->execute($loader->getFixtures());

    }
        public function testVerifyIfInitialisationObjectIsOk()
   {      
        $this->assertNotNull($this->manager);         
        $this->assertSame(Chambre::class,get_class($this->getModelChambre()));
   }

    public function testCreateChambre() : void
	{
        $chambre = $this->getModelChambre();
        $this->manager->persist($chambre);
        $this->manager->flush();
        $this->assertEquals(4,count($this->manager->getRepository(Chambre::class)->findAll()));        
        $this->assertEquals(500, $chambre->getPrixChambre());		
		$reloadedchambre = $this->manager->getRepository(Chambre::class)->find($chambre->getId());
		$this->assertEquals($reloadedchambre, $chambre);
		$this->assertTrue($chambre->getPrixChambre() === $reloadedchambre->getPrixChambre());
    }
    
        public function testUpdateChambre() : void
	{
        $chambre = $this->getModelChambre();
        $chambre->setPrixChambre(700);
        $this->manager->persist($chambre);
        $this->manager->flush();
        $reloadedchambre = $this->manager->getRepository('App\Entity\Chambre')->findOneBy(['id' => $chambre->getId()]);
        $this->assertEquals(4,count($this->manager->getRepository(Chambre::class)->findAll()));        
        
		$this->assertEquals(700, $reloadedchambre->getPrixChambre());
    }
    
         public function testDeleteChambre() : void
	{
        $reloadedchambre = $this->manager->getRepository(Chambre::class)->findAll();
        $this->manager->remove($reloadedchambre[0]);
        $this->manager->flush();
        $this->assertEquals(2,count($this->manager->getRepository(Chambre::class)->findAll()));        
	}

	/**
	 * @return Chambre
	 */
	protected function getModelChambre()
	{
		    $chambre = new Chambre();
            $chambre->setNumberChambre(rand(1,200));
            $chambre->setIdHotel(rand(1,5));
            $chambre->setEtageChambre(rand(1,20));
            $chambre->setNumberLitChambre(rand(1,5));
            $chambre->setPrixChambre(500);
            $chambre->setDescriptionChambre(uniqid("DESCRIPTION UNIQUE"));
            $chambre->setStatusChambre(true);

            return $chambre;
	}

}