<?php

namespace App\Controller;

use App\Entity\Chambre;
use App\Form\ChambreType;
use App\Repository\ChambreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/chambre")
 */
class ChambreController extends Controller
{
    /**
     * @Route("/", name="chambre_index", methods={"GET"})
     */
    public function indexChambreAction(ChambreRepository $chambreRepository): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($chambreRepository->findAll(), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/new", name="chambre_new", methods={"GET","POST"})
     */
    public function newChambreAction(Request $request): Response
    {
       try {
        //On recupère les données depuis le formulaire
        $data = $request->getContent();
        //On les décodent
        $chambre = $this->get('jms_serializer')->deserialize($data, Chambre::class, 'json');
        //On insert les infos dans la database      
        $em = $this->getDoctrine()->getManager();
        $em->persist($chambre);
        $em->flush();
                return new Response('', Response::HTTP_CREATED);
        } catch (NotFoundHttpException $e) {
            echo 'Exception reçue  : ',  $e->getMessage(), "\n";
        }

    }

    /**
     * @Route("/{id}", name="chambre_show", methods={"GET"})
     */
    public function showChambreAction(Chambre $chambre): Response
    {
        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($chambre, 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="chambre_edit", methods={"GET","POST"})
     */
    public function editChambreAction(Request $request, Chambre $chambre): Response
    {
        
        $em = $this->getDoctrine()->getManager();
        $chambreBdd = $em->getRepository('App\Entity\Chambre')->findOneBy(['id' => $chambre->getId()]);      

       if ($chambreBdd) {
            $data = $request->getContent();
            $chambre = $this->get('jms_serializer')->deserialize($data, Chambre::class, 'json');
           
            $chambreBdd->getNumberChambre($chambre->getNumberChambre());
            $chambreBdd->setEtageChambre($chambre->getEtageChambre());
            $chambreBdd->setNumberLitChambre($chambre->getNumberLitChambre());
            $chambreBdd->setDescriptionChambre($chambre->getDescriptionChambre());
            $chambreBdd->setStatusChambre($chambre->getStatusChambre());
            $chambreBdd->setPrixChambre($chambre->getPrixChambre());
            $chambreBdd->setIdHotel($chambre->getIdHotel());
            $chambreBdd->setCategorieChambre($chambre->getCategorieChambre());
            $em->flush();
            return new Response("update succès", Response::HTTP_ACCEPTED);
       }
        return new Response("update echec", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/{id}", name="chambre_delete", methods={"DELETE"})
     */
    public function deleteChambreAction(Request $request, Chambre $chambre): Response
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chambre);
            $em->flush();
            return new Response("delete succès", Response::HTTP_ACCEPTED);
    }

    /**
     * @Route("/hotel/{id}", name="chambre_by_hotel", methods={"GET"})
     */
    public function getChambreByIdHotelAction(ChambreRepository $chambreRepository, $id): Response
    {
        $serializer = $this->get('jms_serializer');      
        $data = $serializer->serialize($chambreRepository->findBy(['idHotel' => $id]), 'json');
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/changestatus/{id}/{st}", name="changestatus_chambre", methods={"POST","GET"})
     */
    public function changeStatusAction($id,$st){
        $em = $this->getDoctrine()->getManager();
        $chambreBdd = $em->getRepository('App\Entity\Chambre')->findOneBy(['id' => $id]); 
        $chambreBdd->setStatusChambre((boolean)$st);
        $em->persist($chambreBdd);
        $em->flush();
        return new Response("Statut modifié avec succès");
    }
}
